/// Generated by AssetsRefGenerator on 2021/09/16
class Res {
  static const String empty = "assets/anim/empty.json";
  static const String airConditioner = "assets/images/airConditioner.png";
  static const String airconditioning = "assets/images/airconditioning.png";
  static const String applepay = "assets/images/applepay.png";
  static const String camraa = "assets/images/camraa.png";
  static const String cashpayment = "assets/images/cashpayment.png";
  static const String cctv = "assets/images/cctv.png";
  static const String confirmorders = "assets/images/confirmorders.png";
  static const String contactus = "assets/images/contactus.png";
  static const String distributionBoard = "assets/images/distributionBoard.png";
  static const String electric = "assets/images/electric.png";
  static const String energy = "assets/images/energy.png";
  static const String handSaw = "assets/images/handSaw.png";
  static const String icon = "assets/images/icon.png";
  static const String iconVisibility = "assets/images/iconVisibility.png";
  static const String lightBolt = "assets/images/lightBolt.png";
  static const String lightbulb = "assets/images/lightbulb.png";
  static const String logo = "assets/images/logo.png";
  static const String paint = "assets/images/paint.png";
  static const String paintRoller = "assets/images/paintRoller.png";
  static const String saudiarabia = "assets/images/saudiarabia.png";
  static const String sink = "assets/images/sink.png";
  static const String splash = "assets/images/splash.png";
  static const String usa = "assets/images/usa.png";
  static const String visa = "assets/images/visa.png";
  static const String wallet = "assets/images/wallet.png";
  static const String wallet2 = "assets/images/wallet2.png";
  static const String wallet3 = "assets/images/wallet3.png";
  static const String water = "assets/images/water.png";
  static const String wifi = "assets/images/wifi.png";
  static const String wifiBack = "assets/images/wifiBack.png";
  static const String wood = "assets/images/wood.png";
  // static const String ar-EG = "assets/langs/ar-EG.json";
  // static const String en-US = "assets/langs/en-US.json";
}
